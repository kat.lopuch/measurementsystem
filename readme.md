# Measurement system

The application was deployed on Google Cloud Platform and is exposed on the following addresses:

- frontend: https://meteo-mars-3vbisao6oa-lz.a.run.app
- backend: https://meteo-mars-backend-3vbisao6oa-lz.a.run.app
- backend authorization: https://meteo-mars-backend-auth-3vbisao6oa-lz.a.run.app

The first load of the mentioned webpages can take up to a few minutes.

## Description

The system consists of the following subsystems:

- measurement device
- measurement service

Responsibilities of the measurement device:

- configuring measurement type: temperature, pressure and humidity,
- executing measurement with the current configuration,
- allowing the access to measurement values.

Responsibilities of the measurement service:

- configuring the measurement device,
- viewing measurement values received from the device.

Communication between the measurement service and the measurements device happens via request and response [messages](#messages-definition).

# Messages definition

The service app controls the device by sending request messages.
The action, which must be performed by the device, is defined as operation type.
As a result, the device sends back response messages. The messages format is **JSON**.

Every message has at least these properties:

- message_type:
  - 0 - request message,
  - 1 - response message,
- operation_type:
  - 0 - configure measurement type,
  - 1 - execute measurement,
  - 2 - get last measurement,
  - 3 - terminate

## Request messages

| properties   | value | description |
| ------------ | ----- | ----------- |
| message_type | 0     | request     |

### Message configuration - request

---

| properties     | value | description                        |
| -------------- | ----- | ---------------------------------- |
| message_type   | 0     | request                            |
| operation_type | 0     | configure the settings to measure: |
| measure_type   | 0     | temperature                        |
|                | 1     | pressure                           |
|                | 2     | humidity                           |

Possible messages:

- temperature: \
  _{"message_type" : 0, "operation_type" : 0, "measure_type" : 0}_
- pressure: \
  _{"message_type" : 0, "operation_type" : 0, "measure_type" : 1}_
- humidity: \
  _{"message_type" : 0, "operation_type" : 0, "measure_type" : 2}_

### Message execute measurement - request

---

| properties     | value | description         |
| -------------- | ----- | ------------------- |
| message_type   | 0     | request             |
| operation_type | 1     | execute measurement |

Possible messages:\
_{"message_type" : 0, "operation_type" : 1}_

### Message get last measurement - request

---

| properties     | value | description          |
| -------------- | ----- | -------------------- |
| message_type   | 0     | request              |
| operation_type | 2     | get last measurement |

Possible messages:\
_{"message_type" : 0, "operation_type" : 2}_

### Message end execution - request

---

| properties     | value | description   |
| -------------- | ----- | ------------- |
| message_type   | 0     | request       |
| operation_type | 3     | end execution |

Possible messages:\
_{"message_type" : 0, "operation_type" : 3}_\
<br>

## Response messages

| properties   | value | description |
| ------------ | ----- | ----------- |
| message_type | 1     | response    |

### Message configuration - response

---

| properties     | value | description                       |
| -------------- | ----- | --------------------------------- |
| message_type   | 1     | response                          |
| operation_type | 0     | configure the settings to measure |
| status         | 0     | ok                                |
|                | 1     | error                             |

Possible messages:

- operation was successful: \
  _{"message_type" : 1, "operation_type" : 0, "status" : 0}_
- operation was not successful: \
  _{"message_type" : 1, "operation_type" : 0, "status" : 1}_

### Message execute measurement - response

---

| properties     | value | description         |
| -------------- | ----- | ------------------- |
| message_type   | 1     | response            |
| operation_type | 1     | execute measurement |
| status         | 0     | ok                  |
|                | 1     | error               |

Possible messages:

- operation was successful: \
  _{"message_type" : 1, "operation_type" : 1, "status" : 0}_
- operation was not successful: \
  _{"message_type" : 1, "operation_type" : 1, "status" : 1}_

### Message get last measurement - response

---

| properties     | value | description              |
| -------------- | ----- | ------------------------ |
| message_type   | 1     | response                 |
| operation_type | 2     | get last measurement     |
| status         | 0     | ok                       |
|                | 1     | error                    |
| measure_type   | 0     | temperature              |
|                | 1     | pressure                 |
|                | 2     | humidity                 |
| measured_value | int   | measured value as number |

Possible messages:

- operation was successful: \
  _{"message_type": 1, "operation_type": 2, "status": 0, "measure_type": 0, "measured_value": 112}_
  _{"message_type": 1, "operation_type": 2, "status": 0, "measure_type": 1, "measured_value": 112}_
  _{"message_type": 1, "operation_type": 2, "status": 0, "measure_type": 2, "measured_value": 112}_
- operation was not successful: \
  _{"message_type": 1, "operation_type": 2, "status": 1, "measure_type": 0, "measured_value": 112}_
  _{"message_type": 1, "operation_type": 2, "status": 1, "measure_type": 1, "measured_value": 112}_
  _{"message_type": 1, "operation_type": 2, "status": 1, "measure_type": 2, "measured_value": 112}_

### Message end execution - response

---

| properties     | value | description   |
| -------------- | ----- | ------------- |
| message_type   | 1     | response      |
| operation_type | 3     | end execution |
| status         | 0     | ok            |
|                | 1     | error         |

Possible messages:

- operation was successful: \
  _{"message_type" : 1, "operation_type" : 3, "status" : 0}_
- operation was not successful: \
   _{"message_type" : 1, "operation_type" : 3, "status" : 1}_
  <br>
