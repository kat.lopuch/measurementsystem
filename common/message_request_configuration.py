from common.message import Message
import json


class MessageRequestConfiguration(Message):
    def __init__(self, message_type: int, operation_type: int, measure_type: int) -> None:
        super().__init__(message_type, operation_type)
        self.measure_type = measure_type

    @staticmethod
    def FromJson(json_text: str) -> 'MessageRequestConfiguration':
        messageRequestFromJson = json.loads(json_text)
        messageRequest = MessageRequestConfiguration("", "", "")
        messageRequest.__dict__.update(messageRequestFromJson)
        return messageRequest
