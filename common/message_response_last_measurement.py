from common.message_response import MessageResponse
import json


class MessageResponseLastMeasurement(MessageResponse):
    def __init__(self, message_type: int, operation_type: int, status: int, measure_type: int, measured_value: int) -> None:
        super().__init__(message_type, operation_type, status)
        self.measure_type = measure_type
        self.measured_value = measured_value

    @staticmethod
    def FromJson(json_text: str) -> 'MessageResponseLastMeasurement':
        messageResponseFromJson = json.loads(json_text)
        messageResponse = MessageResponseLastMeasurement("", "", "", "", "")
        messageResponse.__dict__.update(messageResponseFromJson)
        return messageResponse
