from common.message_response import MessageResponse
from common.message_response_last_measurement import MessageResponseLastMeasurement


class MessageFactoryResponse:
    @staticmethod
    def CreateConfigureMeasurement() -> MessageResponse:
        return MessageResponse

    @staticmethod
    def CreateExecuteMeasure() -> MessageResponse:
        return MessageResponse

    @staticmethod
    def CreateGetLastMeasurement() -> MessageResponseLastMeasurement:
        return MessageResponseLastMeasurement

    @staticmethod
    def CreateEndExecution() -> MessageResponse:
        return MessageResponse
