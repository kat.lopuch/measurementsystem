from common.message import Message
import json


class MessageResponse(Message):
    def __init__(self, message_type: int, operation_type: int, status: int) -> None:
        super().__init__(message_type, operation_type)
        self.status = status

    def toJson(self) -> str:
        return json.dumps(
            self,
            default=lambda o: o.__dict__,
            indent=4)

    def __str__(self) -> str:
        return self.toJson()

    @staticmethod
    def FromJson(jsonText: str) -> 'MessageResponse':
        messageResponseFromJson = json.loads(jsonText)
        messageResponse = MessageResponse("", "", "")
        messageResponse.__dict__.update(messageResponseFromJson)
        return messageResponse
