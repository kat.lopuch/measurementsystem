class Measurement:
    def __init__(self, aType: int, aValue: int) -> None:
        self.type = aType
        self.value = aValue

    def toStr(self) -> str:
        return str(self.type) + ' ' + str(self.value)

    def fromStr(self, strValue: str) -> None:
        strValue_list = strValue.split()
        self.type = int(strValue_list[0])
        self.value = int(strValue_list[1])

    @staticmethod
    def FromStr(strValue: str) -> 'Measurement':
        strValue_list = strValue.split()
        operation_type = int(strValue_list[0])
        value = int(strValue_list[1])
        return Measurement(operation_type, value)
