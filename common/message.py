import json


class Message:
    def __init__(self, message_type: int, operation_type: int) -> None:
        self.message_type = message_type
        self.operation_type = operation_type

    def toJson(self) -> str:
        return json.dumps(
            self,
            default=lambda o: o.__dict__,
            indent=4)

    def __str__(self) -> str:
        return self.toJson()

    @staticmethod
    def FromJson(jsonText: str) -> 'Message':
        messageFromJson = json.loads(jsonText)
        message = Message("", "")
        message.__dict__.update(messageFromJson)
        return message
