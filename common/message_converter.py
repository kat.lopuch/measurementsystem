from common.message import Message
from common.message_factory_request import MessageFactoryRequest
from common.message_factory_response import MessageFactoryResponse


class MessageConverter:

    @staticmethod
    def ConvertJsonTextToMessageObject(jsonText: str) -> Message:
        operationTypeToMessageFactoryRequest = {
            0x00: MessageFactoryRequest.CreateConfigureMeasurement,
            0x01: MessageFactoryRequest.CreateExecuteMeasure,
            0x02: MessageFactoryRequest.CreateGetLastMeasurement,
            0x03: MessageFactoryRequest.CreateEndExecution
        }

        operationTypeToMessageFactoryResponse = {
            0x00: MessageFactoryResponse.CreateConfigureMeasurement,
            0x01: MessageFactoryResponse.CreateExecuteMeasure,
            0x02: MessageFactoryResponse.CreateGetLastMeasurement,
            0x03: MessageFactoryResponse.CreateEndExecution
        }

        messageTypeToFactory = {
            0x00: operationTypeToMessageFactoryRequest,
            0x01: operationTypeToMessageFactoryResponse
        }

        message = ()

        try:
            message = Message.FromJson(jsonText)
            message = messageTypeToFactory[message.message_type][message.operation_type]().FromJson(
                jsonText)
        except:
            raise Exception('Incorrect json text')

        return message
