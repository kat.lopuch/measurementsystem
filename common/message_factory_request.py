from common.message import Message
from common.message_request_configuration import MessageRequestConfiguration


class MessageFactoryRequest:
    @staticmethod
    def CreateConfigureMeasurement() -> MessageRequestConfiguration:
        return MessageRequestConfiguration

    @staticmethod
    def CreateExecuteMeasure() -> Message:
        return Message

    @staticmethod
    def CreateGetLastMeasurement() -> Message:
        return Message

    @staticmethod
    def CreateEndExecution() -> Message:
        return Message
