from common.message import Message
import json


class MessageCofiguration(Message):
    def __init__(self, messageType: int, operationType: int, measureType: int) -> None:
        super().__init__(messageType, operationType)
        self.measure_type = measureType

    @staticmethod
    def FromJson(jsonText: str) -> 'MessageCofiguration':
        messageConfigFromJson = json.loads(jsonText)
        messageConfiguration = MessageCofiguration("", "", "")
        messageConfiguration.__dict__.update(messageConfigFromJson)
        return messageConfiguration
