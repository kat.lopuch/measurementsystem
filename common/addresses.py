import json


class Addresses:
    def __init__(self, ipService: int, portService: int, ipDevice: int, portDevice: int) -> None:
        self.ipService = ipService
        self.portService = portService
        self.ipDevice = ipDevice
        self.portDevice = portDevice

    def toJson(self):
        return json.dumps(
            self,
            default=lambda o: o.__dict__,
            indent=4)

    @staticmethod
    def FromJson(jsonText: str) -> 'Addresses':
        addressesFromJson = json.loads(jsonText)
        addresses = Addresses("127.0.0.1", 8080, "127.0.0.1", 8080)
        addresses.__dict__.update(addressesFromJson)

        return addresses

    @staticmethod
    def FromJsonFile(pathToAddresses: str):
        with open(pathToAddresses, 'r') as fdAddresses:
            addressesJsonText = fdAddresses.read()
            return Addresses.FromJson(addressesJsonText)
