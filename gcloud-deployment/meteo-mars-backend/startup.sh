envsubst '${PORT}' < /home/app/measure_service_react_js/backend/nginx/default > /etc/nginx/sites-available/default
service nginx --full-restart

cd /home/app/measurement-device/
/usr/bin/python3 /home/app/measurement-device/main_app_device.py &

cd /home/app/measure_service_react_js/backend
npm start devStart