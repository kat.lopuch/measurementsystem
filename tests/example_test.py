import unittest

class Test_Example(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print('setUpClass    - begin of a test suit.')

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass - end of a test suit.')

    def setUp(self):
        print('setUp         - begin of a test case.')

    def tearDown(self):
        print('tearDown      - end of a test case.')

    def test_passExample(self):
        print('test_passExample - test case.')
        self.assertEqual(1, 1, "Pass example")

    # def test_failExample(self):
    #     print('test_failExample - test case.')
    #     self.assertEqual(1, 0, "Fail example")