import unittest
import os
from measure_device.data_manager  import DataManager
from common.measurement           import Measurement
from measure_device.configuration import Configuration

class Test_DataManager(unittest.TestCase):

    def setUp(self):
        self.data_manager = DataManager('data')
        
    
    def tearDown(self):
        os.remove('data/configuration.txt')
        os.remove('data/measurements.txt')
    
    def test_init(self):
        self.assertTrue(os.path.exists('data/configuration.txt'), "File doesn't exist")
        self.assertTrue(os.path.exists('data/measurements.txt' ), "File doesn't exist")

    def test_getConfiguration(self):
        self.data_manager.saveConfiguration(Configuration(200))
        self.assertEqual(self.data_manager.getConfiguration().measurementType, 200, "Incorrect measurement type")
        self.data_manager.saveConfiguration(Configuration(67))
        self.assertEqual(self.data_manager.getConfiguration().measurementType, 67, "Incorrect measurement type")

    def test_getLastMeasurement(self):
        self.data_manager.saveMeasurement(Measurement(aType = 500, aValue = 700))
        lastMeasurement = self.data_manager.getLastMeasurement()
        self.assertEqual(lastMeasurement.type , 500, "Incorrect measurement type")
        self.assertEqual(lastMeasurement.value, 700, "Incorrect measurement value")

        self.data_manager.saveMeasurement(Measurement(aType = 1234, aValue = 987))
        lastMeasurement = self.data_manager.getLastMeasurement()
        self.assertEqual(lastMeasurement.type , 1234, "Incorrect measurement type")
        self.assertEqual(lastMeasurement.value, 987, "Incorrect measurement value")