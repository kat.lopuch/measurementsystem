import unittest
import threading
import time
import os
from measure_device.measure_device import MeasureDevice
from measure_service.communicator_measure_service import CommunicatorMeasureService
from common.message_factory_request import MessageFactoryRequest


ComMeasureService = CommunicatorMeasureService()


def MeasureDeviceStart():
    measureDevice = MeasureDevice()
    measureDevice.startWork()

    print("Finished - MeasureDeviceStart")


def ListenForMessages():
    responseMessages = []
    responseMessages.append(ComMeasureService.getMessageResponse())
    responseMessages.append(ComMeasureService.getMessageResponse())
    responseMessages.append(ComMeasureService.getMessageResponse())
    responseMessages.append(ComMeasureService.getMessageResponse())

    operationTypesToResponseReceived = {
        0x00: False,
        0x01: False,
        0x02: False,
        0x03: False
    }

    for responseMessage in responseMessages:
        operationType = responseMessage.operation_type
        if operationType in operationTypesToResponseReceived:
            operationTypesToResponseReceived[operationType] = True

    if not all(value == True for value in operationTypesToResponseReceived.values()):
        raise Exception("Not all types of expected messages were received")

    print("Finished - ListenForMessages")


def SendMessages():
    ComMeasureService.addMessageRequest(MessageFactoryRequest.CreateConfigureMeasurement().FromJson(
        r'{"message_type" : 0, "operation_type" : 0, "measure_type" : 2}'))
    ComMeasureService.addMessageRequest(MessageFactoryRequest.CreateExecuteMeasure().FromJson(
        r'{"message_type" : 0, "operation_type" : 1}'))
    ComMeasureService.addMessageRequest(MessageFactoryRequest.CreateGetLastMeasurement().FromJson(
        r'{"message_type" : 0, "operation_type" : 2}'))
    ComMeasureService.addMessageRequest(MessageFactoryRequest.CreateEndExecution().FromJson(
        r'{"message_type" : 0, "operation_type" : 3}'))


class MeasureDevice_Test(unittest.TestCase):

    def tearDown(self):
        os.remove('data/measure_device/configuration.txt')
        os.remove('data/measure_device/measurements.txt')

    def test_measureScenario(self):
        threadMeasureDeviceStart = threading.Thread(target=MeasureDeviceStart)
        threadMeasureDeviceStart.start()
        time.sleep(0.1)     # Wait for full start of the thread including measure device UDP socket

        threadListenForMessages = threading.Thread(target=ListenForMessages)
        threadListenForMessages.start()
        time.sleep(0.1)     # Wait for full start of the thread including response messages UDP socket

        SendMessages()

        threadMeasureDeviceStart.join()
        threadListenForMessages.join()
