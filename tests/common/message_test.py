import unittest
from common.message import Message


class Test_Message(unittest.TestCase):

    def test_FromJson(self):
        message = Message.FromJson(
            r'{"message_type" : 1, "operation_type" : 0}')
        self.assertEqual(message.message_type, 1, "Wrong message type")
        self.assertEqual(message.operation_type, 0, "Wrong operation type")
