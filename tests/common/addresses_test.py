import unittest
from common.addresses import Addresses

class Test_Addresses(unittest.TestCase):

    def test_toJso(self):
        addresses         = Addresses("127.0.0.1", 8080, "127.0.0.2", 8090)
        addressesJson     = addresses.toJson()
        print(addressesJson)
        addressesFromJson = Addresses.FromJson(addressesJson)
        self.assertEqual(addresses.ipService  , addressesFromJson.ipService  , "Wrong IP Service")
        self.assertEqual(addresses.portService, addressesFromJson.portService, "Wrong port Service")
        self.assertEqual(addresses.ipDevice   , addressesFromJson.ipDevice   , "Wrong IP Device")
        self.assertEqual(addresses.portDevice , addressesFromJson.portDevice , "Wrong port Device")