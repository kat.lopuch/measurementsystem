import unittest
from common.message_factory_request import MessageFactoryRequest


class Test_MessageFactoryRequest(unittest.TestCase):

    def test_CreateConfigureMeasurement(self):
        message = MessageFactoryRequest.CreateConfigureMeasurement().FromJson(
            r'{"message_type" : 0, "operation_type" : 0, "measure_type" : 2}')
        self.assertEqual(message.message_type, 0, "Wrong message type")
        self.assertEqual(message.operation_type, 0, "Wrong operation type")
        self.assertEqual(message.measure_type, 2, "Wrong measure type")
