import unittest
from common.message_converter import MessageConverter


class Test_MessageConverter(unittest.TestCase):

    def test_ConvertJsonTextToMessageObject(self):
        messageJsonText = r'{"message_type" : 0, "operation_type" : 0, "measure_type" : 2}'
        print('\n', "messageJsonText: ", messageJsonText)

        MessageConverter.ConvertJsonTextToMessageObject(messageJsonText)
