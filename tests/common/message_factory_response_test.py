import unittest
from common.message_factory_response import MessageFactoryResponse


class Test_MessageFactoryResponse(unittest.TestCase):

    def test_CreateGetLastMeasurement(self):
        message = MessageFactoryResponse.CreateGetLastMeasurement().FromJson(
            r'{"message_type" : 1, "operation_type" : 0, "status" : 0, "measure_type" : 2, "measured_value" : 1}')
        self.assertEqual(message.message_type, 1, "Wrong message type")
        self.assertEqual(message.operation_type, 0, "Wrong operation type")
        self.assertEqual(message.status, 0, "Wrong status")
        self.assertEqual(message.measure_type, 2, "Wrong measure type")
        self.assertEqual(message.measured_value, 1, "Wrong measured type")
