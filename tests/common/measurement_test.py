import unittest
from common.measurement import Measurement

class Test_Measurement(unittest.TestCase):

    def test_fromStr(self):
        measurement = Measurement(10, 9)
        measurement.fromStr('1 2') 
        print('\ntype:', measurement.type, '\nvalue:', measurement.value)
        self.assertEqual(measurement.type , 1, "Wrong type")
        self.assertEqual(measurement.value, 2, "Wrong value")

    def test_toStr(self):
        measurement1 = Measurement(100, 200)
        measurement2 = Measurement(0, 0)
        measurement2.fromStr(measurement1.toStr())
        self.assertEqual(measurement2.type , 100, "Wrong type")
        self.assertEqual(measurement2.value, 200, "Wrong value")