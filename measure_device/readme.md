# Measurement device

The measurement device is controlled by request messages sent by the measurement service using UDP/IP sockets. The configuration of IP addresses and ports for service and device is stored in `data/addresses.json` file.

The measurement device:
- must be run before the measurement service,
- UPD/IP server listens for a [request message](../readme.md#request-messages),
- each message is handled depending on the operation type property,
- UPD/IP client sends a [response message](../readme.md#response-messages) as a result of handled operation.

## Responsibility: configure measurement type

The measurement device can be configured for measuring temperature, pressure or humidity. Only one measurement type is operative at the moment.

1. [The request message with configuration](../readme.md#message-configuration---request) is received from the service.
2. The `measure_type` property is read from the received message.
3. The `measure_type` value is saved as the current measurement type.
4. The [response message](../readme.md#message-configuration---response) is sent. 
  
Remember that measurement is not executed and the response does not include measurement value.

## Responsibility: executing measurement

Executing measurement and getting the result of this measurement are two separate steps.

1. [The request message with execute measurement](../readme.md#message-execute-measurement---request) is received from the service.
2. The measurement with current configuration is executed.
3. The recent measurement value and type are saved in the `data/measure_device/measurements.txt`.
4. The [response message](../readme.md#message-execute-measurement---response) with the status of the operation is sent.

## Responsibility: allowing the access to measurement values

1. [The request message with get last measurement](../readme.md#message-get-last-measurement---request) is received from the service.
2. The last measurement type and value are read from the file `data/measure_device/measurements.txt`.
3. The [response message](../readme.md#message-get-last-measurement---response) with the status, measurement type and measurement value is sent.

## Example scenario

1. `main_app_device.py` is started.
2. Communicator, data manager and configuration are initialized.
3. UDP server socket is initialized with data from `data/addresses.json`.
4. UDP server socket is waiting for a request message.
5. Message is received. The content is in JSON format.
6. JSON message is converted to object of Message class.
7. Object of Message class is handled depending on the operation type value.
8. Response is prepared.
9. Response is converted to JSON format.
10. Response JSON is sent.
   - In case of Message with end-execution request, execution is terminated.
11. Executions is continued from the point 4.

## How to run - measurement device

1. In terminal, change directory to the folder *`measurementsystem`*.
2. Fill *`data/addresses.json`* file with Service IP, Device IP and ports.
3. Run `main_app_device.py` in terminal

```
$ python main_app_device.py
```