import os
from measure_device.configuration import Configuration
from common.measurement import Measurement


class DataManager:
    def __init__(self, pathToData: str) -> None:
        self.pathToData = pathToData
        self.pathToDataMeasurements = pathToData + '/measurements.txt'
        self.pathToDataConfiguration = pathToData + '/configuration.txt'

        if not os.path.isfile(self.pathToDataMeasurements):
            open(self.pathToDataMeasurements, 'w').close()

        if not os.path.isfile(self.pathToDataConfiguration):
            configration_file = open(self.pathToDataConfiguration, 'w')
            configration_file.writelines('0')
            configration_file.close()

    def getConfiguration(self) -> Configuration:
        with open(self.pathToDataConfiguration, 'r') as fdConfiguration:
            file_lines = fdConfiguration.readlines()

        return Configuration.FromStr(file_lines[0])

    def saveConfiguration(self, aCongfiguration: Configuration) -> None:
        with open(self.pathToDataConfiguration, 'w') as fdConfiguration:
            fdConfiguration.write(aCongfiguration.toStr())

    def saveMeasurement(self, aMeasurement: Measurement) -> None:
        with open(self.pathToDataMeasurements, 'r+') as fdMeasurement:
            if len(fdMeasurement.readlines()) != 0:
                fdMeasurement.write('\n' + aMeasurement.toStr())
            else:
                fdMeasurement.write(aMeasurement.toStr())

    def getLastMeasurement(self) -> Measurement:
        fdMeasurement = open(self.pathToDataMeasurements, 'r')
        last_line = fdMeasurement.readlines()[-1]
        fdMeasurement.close()
        return Measurement.FromStr(last_line)
