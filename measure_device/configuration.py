class Configuration:
    def __init__(self, measurementType: int) -> None:
        self.measurementType = measurementType

    def toStr(self) -> str:
        return str(self.measurementType)

    @staticmethod
    def FromStr(strValue: str) -> 'Configuration':
        configuration = Configuration(int(strValue))
        return configuration
