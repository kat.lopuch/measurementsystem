from common.message import Message
from common.message_response_last_measurement import MessageResponseLastMeasurement
from measure_device.communicator_measure_device import CommunicatorMeasureDevice
from measure_device.data_manager import DataManager
from common.measurement import Measurement
from common.message_response import MessageResponse
import random

STATUS_OK = 0
STATUS_ERROR = 1


class MeasureDevice:
    def __init__(self) -> None:
        self.communicatorMeasureDevice = CommunicatorMeasureDevice()
        self.dataManager = DataManager(pathToData='data/measure_device')
        self.configuration = self.dataManager.getConfiguration()

    def measure(self) -> None:
        value = random.randrange(200)
        measurement = Measurement(aType=self.configuration.measurementType, aValue=value)
        self.dataManager.saveMeasurement(measurement)

    def configure(self, aMeasureType: int) -> None:
        self.configuration.measurementType = aMeasureType
        self.dataManager.saveConfiguration(aCongfiguration=self.configuration)

    def getLastMeasurement(self) -> Measurement:
        measurement = self.dataManager.getLastMeasurement()
        return measurement

    def handleMessage(self, message: Message) -> None:

        if message.operation_type == 0x00:
            messageResponse = MessageResponse(
                message_type=0x01, operation_type=0x00, status=STATUS_OK)
            try:
                self.configure(aMeasureType=message.measure_type)
            except:
                messageResponse.status = STATUS_ERROR

            return self.communicatorMeasureDevice.addMessageResponse(messageResponse)

        elif message.operation_type == 0x01:
            messageResponse = MessageResponse(
                message_type=0x01, operation_type=0x01, status=STATUS_OK)
            try:
                self.measure()
            except:
                messageResponse.status = STATUS_ERROR

            return self.communicatorMeasureDevice.addMessageResponse(messageResponse)

        elif message.operation_type == 0x02:
            messageResponse = MessageResponseLastMeasurement(
                message_type=0x01, operation_type=0x02, status=STATUS_OK, measure_type=0x00, measured_value=0x00)
            try:
                lastMeasuremet = self.getLastMeasurement()
                messageResponse.measure_type = lastMeasuremet.type
                messageResponse.measured_value = lastMeasuremet.value
            except:
                messageResponse.status = STATUS_ERROR

            return self.communicatorMeasureDevice.addMessageResponse(messageResponse)

        elif message.operation_type == 0x03:
            messageResponse = MessageResponse(
                message_type=0x01, operation_type=0x03, status=STATUS_OK)
            self.communicatorMeasureDevice.addMessageResponse(messageResponse)
            exit()

    def startWork(self) -> None:

        communicator = self.communicatorMeasureDevice
        print("Measurement service: \n\tIP address - ", communicator.serviceIpAddressAndPort[0],
              ", port - ", communicator.serviceIpAddressAndPort[1],
              "\nMeasurement device: \n\tIP address - ", communicator.deviceIpAddressAndPort[0],
              ", port - ", communicator.deviceIpAddressAndPort[1])

        while True:
            try:
                receivedMessage = communicator.getMessageRequest()
                print("\n[Measurement device] Received message:\n", receivedMessage, sep="")
                self.handleMessage(receivedMessage)
            except:
                return
