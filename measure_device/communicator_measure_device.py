from common.message import Message
from common.message_converter import MessageConverter
from common.addresses import Addresses
import socket

from common.message_response import MessageResponse

STATUS_ERROR = 1


class CommunicatorMeasureDevice:

    def __init__(self) -> None:
        addresses = Addresses.FromJsonFile('data/addresses.json')
        self.serviceIpAddressAndPort = (addresses.ipService, addresses.portService)
        self.deviceIpAddressAndPort = (addresses.ipDevice, addresses.portDevice)
        self.socketSender = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
        self.socketReceiver = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

        self.socketReceiver.bind(self.deviceIpAddressAndPort)

    def _receiveMessage(self) -> Message:
        receiveMessage = self.socketReceiver.recvfrom(1024)
        return MessageConverter.ConvertJsonTextToMessageObject(receiveMessage[0].decode())

    def _sendMessage(self, aResponseMessage: MessageResponse) -> None:
        print("\n[Measurement device] Response message:\n", aResponseMessage, sep="")

        responseMessageBytes = str.encode(aResponseMessage.toJson())
        self.socketSender.sendto(responseMessageBytes, self.serviceIpAddressAndPort)

    def addMessageResponse(self, aMessage: MessageResponse) -> None:
        self._sendMessage(aResponseMessage=aMessage)

    def getMessageRequest(self) -> Message:
        return self._receiveMessage()
