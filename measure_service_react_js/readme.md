# Measurement service - react.js

The measurement service is responsible for:
- exposing frontend implemented with react components,
- handling http requests related to the measurement device,
- interactions with the measurement device using UDP - [messages](../readme.md#messages-definition),
- authentication and authorization of predefined user.

## Backend

Backend consists of two applications:
- measure_service_react_js/server.js
- measure_service_react_js/server_auth.js

Backend is made up of:
- server.js:
  - uses Device Messenger to send and received messages from the measurement device,
  - handles HTTP requests for sending messages and receiving responses,
  - JWT verification when end execution message is sent,
- server_auth.js:
  - handles HTTP requests,
  - authorizes the predefined user,
  - generates a new access token basing on the refresh token.

### The Backend Application 

The starting point of the application is in the following file:
*`measurementsystem/measure_service_react_js/backend/server.js`*

The server is exposed on the port 3000.

#### Device Messengers

It is responsible for basic communication with measurement device 
and saving response messages from the device. There are a few possible 
modes of work when it comes to the Device Messenger: without database 
and with database. The mode of work is chosen basing on Operating System
environment variable: `SAVING_MESSAGES_MODE`, which is located in 
*`measurementsystem/measure_service_react_js/backend/.env`*. 
If the variable is not defined, the default mode is without database. 
The creation of particular Device Messenger is done using Device Messenger Factory. 

#### Device Messenger No DB

The main responsibilities are:
- sending request messages to the measurement device via UDP socket,
- listening for response messages from the measurement device,
- saving responses (up to the last 50 responses) from the measurement device 
  in no persistent way (RAM), hence the messages are gone after application 
  restart when using this particular messenger,
- getting response messages received from the measurement device - reading 
  them from the list saved in RAM,
- removing all device response messages from the list RAM.
  
#### Device Messenger Mongo 

The main responsibilities are:
- sending request messages to the measurement device via UDP socket,
- listening for response messages from the measurement device,
- saving responses (up to the last 50 responses) from the measurement device 
  in a persistent way in the Mongo database, hence the messages should not
  be gone after application restart (the messages are saved on the hard drive),
- getting response messages received from the measurement device - reading 
  them from the database,
- removing all device response messages from the database.

#### REST Endpoints

The server handles such requests for the following endpoints:
- `/device-request-message` [POST] - sends [request message](../readme.md#request-messages) to the measurement device (using DeviceMessenger). In case of end execution message, the valid access token is required,
  - request:
    - headers: `Authorization: Bearer <accessToken>`,
    - body: json with [request message](../readme.md#request-messages),
  - response:
    - headers: default,
    - body:
      - `{"status": "Empty message to device"}`,
      - `{"status": "Access not allowed"}`,
      - `{"status": "Done"}`,
- `/device-response-messages` [GET] - returns list of response messages from the
measurement device (using DeviceMessenger),
  - request:
    - headers: `Authorization: Bearer <accessToken>`,
    - body: `empty`,
  - response:
    - headers: `default`,
    - body: list of [response messages](../readme.md#response-messages) as JSON,
- `/device-response-messages` [DELETE] - removes list of response messages from the measurement device (using DeviceMessenger),
  - request:
    - headers: `Authorization: Bearer <accessToken>`,
    - body: `empty`,
  - response:
    - headers: `default`,
    - body: `status`.

#### WebSocket Endpoint

The WebSocket endpoint is exposed on port 3001.
It sends response messages from the measurement device to the clients of WebSocket immediately after the response message is received by the server.

### The Backend Authorization Application

The starting point of the application is in the following file:
*`measurementsystem/measure_service_react_js/backend/server_auth.js`*

The server is exposed on the port 4000.

#### REST Endpoints

The server handles such requests for the following endpoints:
- `/login` [POST] - authenticates the predefined user. In case of successful authentication, generates refresh and access tokens, which are placed in the response message.
  - request:
    - headers: default,
    - body: `{"username": "<name>", "psw": "<password>"}`,
  - response:
    - headers: default,
    - body:
      - on error:
        - `{"status": "Incorrect user"}`,
        - `{"status": "Internal server error"}`,
      - on success:
        - `{"accessToken":"<JWT token>", "refreshToken":"<JWT token>"}`,
- `/refreshtoken` [POST] - requires refresh token in the header of the request message. In case of successful verification of the refresh token, a new access token is places in the response message.
  - request:
    - headers: `"refresh-token": "<JWT token>"`,
    - body: empty,
  - response:
    - headers: default,
    - body: 
      - on error: empty,
      - on success: `{"accessToken":"<JWT token>"}`,
- `/logout` [DELETE] - signs out the user by deleting the refresh token associated with the predefined user
  - request:
    - headers: `"refresh-token": "<JWT token>"`,
    - body: empty,
  - response:
    - headers: default,
    - body: empty.

## Frontend

The server is exposed on the port 5000.

The frontend consists of the following pages:
- Advanced,
- Simple.
The possibility of changing webpages is implemented with React Rout.

Every page is made up of reusable react components, such as:
- opening video of mars,
- header,
- navigation bar,
- view of the response messages from the measurement device,
- footer.

Individual components on advanced page:
  - login module,
  - buttons panel.
  
Individual components on simple page:
  - weather widget.

### Page: advanced

The page allows to control the device step by step by sending messages using proper buttons.
There is also possibility to login as the predefined user (`username: admin, psw: password`).
The authentication of the predefined user is requested to send the end execution message to the measurement device.

The main content is divided into:
- left side, section with buttons:
  - Configure the measurement device:
    - temperature,
    - pressure,
    - humidity,
  - Execute measurement,
  - Get last measurement,
  - Remove all messages,
  - End execution,
    - header with access token must be included in the request to be authorized,
- right side, section with response messages from the measurement device.

A click action on one of the buttons sends [request message](../readme.md#request-messages). Requests are posted by HTTP POST method to `/device-request-message` endpoint. From the backend endpoint, they are passed to the measurement device.
In case of sending *Remove all messages* request, the HTTP DELETE method is used
and all saved messages from the measurement device are removed.

#### Authentication / authorization

Authentication:
- is done using the server_auth endpoint: `/login` endpoint
- successful authentication responses with JSON object, which contains following fields:
  - `accessToken`: base64url encoded token, expiration time is 10min,
  - `refreshToken`: base64url encoded token, expiration time is 2days,
- access token and refresh token are saved in local storage using such keys:
  - `refreshToken`,
  - `accessToken`.

Authorization:
- sending the end execution message requires authorization hence the `accessToken` must be places in the header of the request message.
- refresh token is used to generate a new access token e.g when access token expires. Generating new access token is done using the `/refreshtoken` endpoint.

The access and refresh tokens are actually JSON Web Tokens signed with symmetric key.
https://datatracker.ietf.org/doc/html/rfc7519

### Page: simple

Measurements of temperature, pressure and humidity are done automatically
and the results are updated in the main content.

The main content consists of:
- section with the measurements:
  - temperature: icon + value
  - pressure: icon + value
  - humidity: icon + value
- section with response messages from the measurement device. By default the section is not visible. The visibility can be toggled by the eye icon.

#### Automatic sending of measurement requests

In the background, there are calls of `measureAll` function. It is called periodical with a given interval (3s). The function sends such request messages for every weather parameter:
- [configure](../readme.md#message-configuration---request),
- [execute measurement](../readme.md#message-execute-measurement---request), 
- [get last measurement](../readme.md#message-get-last-measurement---request).
 
The HTTP POST requests are send to `/device-request-message` endpoint. The backend endpoint forwards the message to the measurement device.

### Automatic getting of measurement results

In the background, there is WebSocket that expects the response messages from the measurement device. When an error occurs or connection is lost there is an attempt to renew the connection periodically

## How to run - react.js
Remember to start the [measurement device](../measure_device/readme.md#how-to-run---measurement-device) before.

1. In the terminal, change directory to the folder *`measure_service_react_js/backend`*
2. In the terminal shell run *`server.js`* 
```
$ npm run devStart 
```
3. In another shell of the terminal run *`server_auth.js`*
```
$ npm run devStartAuth
```
4. In next shell of the terminal, change directory to the folder *`measure_service_react_js/frontend`*.
5. In above shell run
```
$ PORT=5000 npm run start
```