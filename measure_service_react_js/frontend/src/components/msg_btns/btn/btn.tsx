import React from "react";
import "./btn.css";

interface BtnProps {
  class: string;
  text: string;
  func: () => any;
}

function Btn(props: BtnProps) {
  return (
    <button className={`btn ${props.class}`} onClick={props.func}>
      {props.text}
    </button>
  );
}

export default Btn;
