import React, { useEffect, useRef, useState } from "react";
import Btn from "./btn/btn";
import SendMessages from "../tools/message/send_messages";
import BtnImg from "./btn_img/btn_img";
import ConfigArrow from "../../assets/down-chevron.png";
import Login from "../login/login";
import autoAnimate from "@formkit/auto-animate";
import { useInView } from "framer-motion";
import "./msg_btns.css";

interface MsgBtnsProps {
  onShowMsg: (text: string) => void;
  onIsLoggedIn: (value: React.SetStateAction<boolean>) => void;
}

function MsgBtns(props: MsgBtnsProps) {
  const [showConfig, setShowConfig] = useState(false);
  const updateShowConfig = () => setShowConfig(!showConfig);
  const configBtnRef = useRef(null);

  const btnContainerRef = useRef(null);
  const isInView = useInView(btnContainerRef);

  const sendEndMeas = () => {
    if (Login.IsUserLoggedIn()) {
      if (!SendMessages.sendEndMeas()) {
        if (Login.UpdateAccessToken()) {
          SendMessages.sendEndMeas();
        } else {
          // access token and refresh token are not valid for current user
          props.onIsLoggedIn(false);
        }
      }
    } else {
      props.onShowMsg("Option only for logged in user");
    }
  };

  const getConfigBtnArrow = () => {
    let configArrowStyle = "btn_img__config-arrow ";
    configArrowStyle += showConfig
      ? "btn_img__rotate_up"
      : "btn_img__rotate_down";
    return configArrowStyle;
  };

  useEffect(() => {
    configBtnRef.current && autoAnimate(configBtnRef.current);
  }, [configBtnRef]);

  return (
    <div
      style={{
        transform: isInView ? "none" : "translateX(-100px)",
        opacity: isInView ? 1 : 0,
      }}
      className="msg-btns__column"
      id="btn-container"
      ref={btnContainerRef}
    >
      <BtnImg
        text={"Configure the measurement device"}
        func={updateShowConfig}
        img={ConfigArrow}
        alt="Arrow icon"
        imgClass={getConfigBtnArrow()}
      />
      <span ref={configBtnRef}>
        {showConfig && (
          <div className="msg-btns__dropdown">
            <Btn
              class="btn__small"
              text="temperature"
              func={() => SendMessages.sendConfiguration(0)}
            />
            <Btn
              class="btn__small"
              text="pressure"
              func={() => SendMessages.sendConfiguration(1)}
            />
            <Btn
              class="btn__small"
              text="humidity"
              func={() => SendMessages.sendConfiguration(2)}
            />
          </div>
        )}
      </span>

      <Btn
        class="btn__medium"
        text="Execute measurement"
        func={SendMessages.sendExecuteMeas}
      />
      <Btn
        class="btn__medium"
        text="Get last measurement"
        func={SendMessages.sendGetLastMeas}
      />
      <Btn
        class="btn__medium"
        text="Remove all messages"
        func={SendMessages.sendDeleteDeviceRspMessages}
      />
      <Btn class="btn__medium" text="End execution" func={sendEndMeas} />
    </div>
  );
}

export default MsgBtns;
