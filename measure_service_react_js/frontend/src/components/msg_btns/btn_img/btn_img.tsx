import React from "react";
import "../btn/btn.css";
import "./btn_img.css";

interface BtnImgProps {
  func: () => void;
  text: string;
  imgClass: string;
  img: string;
  alt: string;
}

function BtnImg(props: BtnImgProps) {
  return (
    <button className="btn btn__medium" onClick={props.func}>
      {props.text}
      <img className={props.imgClass} src={props.img} alt={props.alt} />
    </button>
  );
}

export default BtnImg;
