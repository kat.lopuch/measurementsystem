import React from "react";
import tempIcon from "../../assets/temperature-icon.svg";
import pressIcon from "../../assets/pressure-icon.svg";
import humIcon from "../../assets/humidity-icon.svg";
import WidgetWeatherRow from "./widget_weather_row";
import MeasuredValues from "../tools/measured_values";
import { useAppSelector } from "../tools/redux/hooks";
import "./widget_weather.css";
import { Validate } from "../tools/message/interfaces/messageResponse";

interface WidgetWeatherProps {
  class: string;
}

function WidgetWeather(props: WidgetWeatherProps) {
  const deviceMsgResponses = useAppSelector(
    (state) => state.deviceMsgResponsesSlice.deviceMsgResponses
  );
  // copy to prevent original array manipulation
  let copyToArray = [...deviceMsgResponses];
  let measuredValues = new MeasuredValues();
  for (let message of copyToArray.reverse()) {
    if (!Validate(message)) continue;

    if (message.measure_type !== undefined) {
      measuredValues.updateValues(message);
    }
  }

  const measurement = measuredValues.getNewestMeasurements();
  measurement.humidity = measurement.humidity ?? "-";
  measurement.pressure = measurement.pressure ?? "-";
  measurement.temperature = measurement.temperature ?? "-";

  return (
    <div className={`widget-weather ${props.class}`}>
      <WidgetWeatherRow
        icon={tempIcon}
        value={measurement.temperature + " \u00b0 C"}
        alt="temperature-icon"
      />
      <WidgetWeatherRow
        icon={pressIcon}
        value={measurement.pressure + " hPa"}
        alt="pressure-icon"
      />
      <WidgetWeatherRow
        icon={humIcon}
        value={measurement.humidity + " %"}
        alt="humidity-icon"
      />
    </div>
  );
}
export default WidgetWeather;
