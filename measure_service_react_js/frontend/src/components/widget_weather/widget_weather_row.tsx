import React from "react";
import "./widget_weather_row.css";

interface WidgetWeatherRowProps {
  icon: string;
  alt: string;
  value: string;
}

function WidgetWeatherRow(props: WidgetWeatherRowProps) {
  return (
    <div className="widget-weather-row">
      <img
        src={props.icon}
        alt={props.alt}
        className="widget-weather-row__icon"
      />
      <div className="widget-weather-row__value"> {props.value} </div>
    </div>
  );
}

export default WidgetWeatherRow;
