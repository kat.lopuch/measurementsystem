import React from "react";
import LoginIcon from "../../../assets/login-icon.svg";
import LogOutIcon from "../../../assets/logout.svg";
import "../navbar.css";
import "./navbar_login.css";

interface NavbarLoginProps {
  isLoggedIn: boolean;
  onLogout: () => void;
  onLogin: () => void;
}

function NavbarLogin(props: NavbarLoginProps) {
  const getIcon = () => {
    return props.isLoggedIn ? LogOutIcon : LoginIcon;
  };

  const getText = () => {
    return props.isLoggedIn ? "Logout" : "Login";
  };

  const onClick = () => {
    props.isLoggedIn ? props.onLogout() : props.onLogin();
  };

  return (
    <div className="navbar__link" onClick={onClick}>
      {getText()}
      <img
        className="navbar-login__login-icon"
        src={getIcon()}
        alt="Login icon"
      />
    </div>
  );
}

export default NavbarLogin;
