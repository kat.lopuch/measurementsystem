import React from "react";
import { Link, useLocation } from "react-router-dom";
import NavbarLogin from "./navbar_login/navbar_login";
import NavbarToggle from "./navbar_toggle/navbar_toggle";
import "./navbar.css";

function isAdvancedPage(path: string) {
  return "/" === path;
}

function isSimplePage(path: string) {
  return "/simple" === path;
}

interface NavbarProps {
  onLogin: () => void;
  onLogout: () => void;
  isLoggedIn: boolean;
  showDeviceMsgResponses: boolean;
  onShowDeviceMsgResponses: (state: boolean) => void;
}

function Navbar(props: NavbarProps) {
  const path = useLocation().pathname;

  return (
    <nav>
      <ul className="navbar">
        <div className="navbar__links">
          <li>
            <Link
              className={
                "navbar__link " + (isAdvancedPage(path) ? "navbar__active" : "")
              }
              to="/"
            >
              Advanced
            </Link>
          </li>
          <li>
            <Link
              className={
                "navbar__link " + (isSimplePage(path) ? " navbar__active" : "")
              }
              to="/simple"
            >
              Simple
            </Link>
          </li>
        </div>
        <div className="navbar__links">
          {isAdvancedPage(path) && (
            <li>
              <NavbarLogin
                onLogin={props.onLogin}
                onLogout={props.onLogout}
                isLoggedIn={props.isLoggedIn}
              />
            </li>
          )}
          {!isAdvancedPage(path) && (
            <li>
              <NavbarToggle
                toggleState={props.showDeviceMsgResponses}
                onToggle={props.onShowDeviceMsgResponses}
              />
            </li>
          )}
        </div>
      </ul>
    </nav>
  );
}

export default Navbar;
