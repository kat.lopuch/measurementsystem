import React from "react";
import Eye from "../../../assets/eye.png";
import CrossedEye from "../../../assets/crossed-eye.png";
import "../navbar.css";
import "./navbar_toggle.css";

interface NavbarToggleProps {
  onToggle: (state: boolean) => void;
  toggleState: boolean;
}

function NavbarToggle(props: NavbarToggleProps) {
  const getIcon = () => {
    return props.toggleState ? CrossedEye : Eye;
  };

  const toggle = () => {
    // update toggleState, which is defined in parent component: view_main.jsx
    props.onToggle(!props.toggleState);
  };

  return (
    <div className="navbar-toggle__wrapper">
      <img
        className="navbar-toggle__rsp-msg-icon"
        src={getIcon()}
        alt="Show icon"
        onClick={toggle}
      />
    </div>
  );
}

export default NavbarToggle;
