import React from "react";
import "./backdrop.css";

interface BackdropProps {
  onClick: () => void;
}

function Backdrop(props: BackdropProps) {
  return <div className="backdrop" onClick={props.onClick} />;
}

export default Backdrop;
