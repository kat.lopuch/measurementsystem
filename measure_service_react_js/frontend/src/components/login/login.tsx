import React from "react";
import CloseIcon from "../../assets/close.svg";
import LoginIcon from "../../assets/login.svg";
import LockIcon from "../../assets/lock.svg";
import { getBackendAuthUrl } from "../config/config";
import "./login.css";

interface LoginProps {
  onHide: () => void;
  onShowMsg: (text: string) => void;
  onIsLoggedIn: (value: React.SetStateAction<boolean>) => void;
}

interface LoginState {
  username: string;
  psw: string;
}

class Login extends React.Component<LoginProps, LoginState> {
  constructor(props: LoginProps) {
    super(props);
    this.state = {
      username: "",
      psw: "",
    };
  }

  static IsUserLoggedIn() {
    return localStorage.getItem("accessToken") !== null;
  }

  static UpdateAccessToken: () => boolean | void = () => {
    const address = getBackendAuthUrl();
    fetch(address + "/refreshtoken", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "refresh-token": JSON.parse(localStorage.getItem("refreshToken") || ""),
      },
    }).then((responseRefreshToken) => {
      responseRefreshToken.json().then((data) => {
        if (data.accessToken != null) {
          localStorage.setItem("accessToken", data.accessToken);
          return true;
        }
        return false;
      });
    });
  };

  updateInputUsername = (event: React.ChangeEvent<HTMLInputElement>) => {
    const username = event.target.value;
    this.setState({ username: username });
  };

  updateInputPsw = (event: React.ChangeEvent<HTMLInputElement>) => {
    const psw = event.target.value;
    this.setState({ psw: psw });
  };

  requestLogin = () => {
    const address = getBackendAuthUrl();
    fetch(address + "/login", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(this.state),
    }).then((response) => {
      response.json().then((data) => {
        console.log(data);
        if (data.accessToken != null) {
          localStorage.setItem("accessToken", data.accessToken);
          localStorage.setItem("refreshToken", data.refreshToken);
          this.props.onShowMsg("You are logged in");
          this.props.onIsLoggedIn(true);
          this.props.onHide();
        }
      });
    });
  };

  render() {
    return (
      <div className="login-area">
        <header className="login-title">
          Log<span>in</span>
          <img
            className="close-icon"
            alt="Close icon"
            src={CloseIcon}
            onClick={this.props.onHide}
          />
        </header>
        <div className="input-area">
          <label htmlFor="username">Username</label>
          <div className="input-container">
            <img className="username-icon" src={LoginIcon} alt="User icon" />
            <input
              className="input-field"
              type="text"
              value={this.state.username}
              onChange={this.updateInputUsername}
              placeholder="Enter username"
              name="username"
              required
            />
          </div>

          <label htmlFor="psw">Password</label>
          <div className="input-container">
            <img className="psw-icon" src={LockIcon} alt="Lock icon" />
            <input
              className="input-field"
              type="password"
              value={this.state.psw}
              onChange={(event) => this.updateInputPsw(event)}
              placeholder="Enter password"
              name="psw"
              required
            />
          </div>

          <div className="btns-area">
            <button
              className="btn cancel-btn"
              type="button"
              onClick={this.props.onHide}
            >
              Cancel
            </button>
            <button
              className="btn login-btn"
              type="button"
              onClick={this.requestLogin}
            >
              Log in
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
