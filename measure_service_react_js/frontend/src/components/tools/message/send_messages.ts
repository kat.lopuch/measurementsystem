import { getBackendUrl } from "../../config/config";
import MessageRequest from "./interfaces/messageRequest";

class SendMessages {
  static async sendDeviceRequestMessage(message: MessageRequest) {
    const address: string = getBackendUrl();
    fetch(address + "/device-request-message", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: `Bearer ${localStorage.getItem("accessToken")}`,
      },
      body: JSON.stringify(message),
    }).then((response) => {
      let data = response.json() as any;
      if (data.status === "Access not allowed") {
        return false;
      }
    });
    return true;
  }

  static sendConfiguration(measureType: number) {
    return SendMessages.sendDeviceRequestMessage({
      message_type: 0,
      operation_type: 0,
      measure_type: measureType,
    });
  }

  static sendExecuteMeas() {
    return SendMessages.sendDeviceRequestMessage({
      message_type: 0,
      operation_type: 1,
    });
  }

  static sendGetLastMeas() {
    return SendMessages.sendDeviceRequestMessage({
      message_type: 0,
      operation_type: 2,
    });
  }

  static sendEndMeas() {
    return SendMessages.sendDeviceRequestMessage({
      message_type: 0,
      operation_type: 3,
    });
  }

  static async sendDeleteDeviceRspMessages() {
    const address = getBackendUrl();
    fetch(address + "/device-response-messages", {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authorization: `Bearer ${localStorage.getItem("accessToken")}`,
      },
    }).then((response) => {
      let data = response.json() as any;
      if (data.status === "Access not allowed") {
        return false;
      }
    });
    return true;
  }

  static measureTemperature() {
    SendMessages.sendConfiguration(0);
    SendMessages.sendExecuteMeas();
    SendMessages.sendGetLastMeas();
  }

  static measurePressure() {
    SendMessages.sendConfiguration(1);
    SendMessages.sendExecuteMeas();
    SendMessages.sendGetLastMeas();
  }

  static measureHumidity() {
    SendMessages.sendConfiguration(2);
    SendMessages.sendExecuteMeas();
    SendMessages.sendGetLastMeas();
  }

  static measureAll() {
    SendMessages.measureTemperature();
    SendMessages.measurePressure();
    SendMessages.measureHumidity();
  }
}

export default SendMessages;
