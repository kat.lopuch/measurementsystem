export default interface MessageRequest {
  message_type: number;
  operation_type: number;
  measure_type?: number;
}
