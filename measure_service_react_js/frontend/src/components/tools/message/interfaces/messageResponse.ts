export default interface MessageResponseFromDevice {
  message_type: number;
  operation_type: number;
  status: number;
  measure_type?: number;
  measured_value?: number;
}

export function Validate(message: MessageResponseFromDevice) {
  if (
    message.message_type === undefined ||
    message.operation_type === undefined
  ) {
    return false;
  }
  return true;
}
