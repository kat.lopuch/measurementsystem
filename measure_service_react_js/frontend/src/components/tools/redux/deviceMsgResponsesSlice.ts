import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { DeviceMsgResponse, DeviceMsgResponses } from "./interfaces";

const initialState: DeviceMsgResponses = { deviceMsgResponses: [] };
export const deviceMsgResponsesSlice = createSlice({
  name: "deviceMsgResponses",
  initialState: initialState,
  reducers: {
    updateDeviceMsgResponses: (
      state,
      action: PayloadAction<{
        deviceMsgResponses: DeviceMsgResponse[];
      }>
    ) => {
      state.deviceMsgResponses = action.payload.deviceMsgResponses;
    },
  },
});

export const { updateDeviceMsgResponses } = deviceMsgResponsesSlice.actions;
export default deviceMsgResponsesSlice.reducer;
