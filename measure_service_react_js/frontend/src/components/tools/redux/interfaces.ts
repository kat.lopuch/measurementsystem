export interface DeviceMsgResponse {
  _id: number;
  message_type: number;
  operation_type: number;
  status: number;
  measure_type?: number;
  measured_value?: number;
}

export interface DeviceMsgResponses {
  deviceMsgResponses: DeviceMsgResponse[];
}
