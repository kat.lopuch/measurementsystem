import { configureStore } from "@reduxjs/toolkit";
import deviceMsgResponsesSlice from "./deviceMsgResponsesSlice";

export const deviceMsgResponsesStorage = configureStore({
  reducer: {
    deviceMsgResponsesSlice: deviceMsgResponsesSlice,
  },
});

export type RootState = ReturnType<typeof deviceMsgResponsesStorage.getState>;
export type AppDispatch = typeof deviceMsgResponsesStorage.dispatch;
