class ResponseAdapterText {
  static GetMessageTypeAsText(messageType: number): string | undefined {
    const valueToText = new Map<number, string>([
      [0, "request"],
      [1, "response"],
    ]);
    return valueToText.get(messageType);
  }

  static GetOperationTypeAsText(operationType: number): string | undefined {
    const valueToText = new Map<number, string>([
      [0, "configure the device"],
      [1, "execute measurement"],
      [2, "get last measurement"],
      [3, "end execution"],
    ]);
    return valueToText.get(operationType);
  }

  static GetStatusTypeAsText(status: number): string | undefined {
    const valueToText = new Map<number, string>([
      [0, "OK"],
      [1, "ERROR"],
    ]);
    return valueToText.get(status);
  }

  static GetMeasureTypeAsText(measureType: number): string | undefined {
    const valueToText = new Map<number, string>([
      [0, "temperature"],
      [1, "pressure"],
      [2, "humidity"],
    ]);
    return valueToText.get(measureType);
  }

  static GetMeasuredValueAsText(
    measureType: string,
    measuredValue: number
  ): string | undefined {
    const unitTable = new Map<string, string>([
      ["temperature", " &deg C"],
      ["pressure", " hPa"],
      ["humidity", " %"],
    ]);
    return measuredValue.toString() + unitTable.get(measureType);
  }
}

export default ResponseAdapterText;
