import { DeviceMsgResponse } from "./redux/interfaces";

class MeasuredValues {
  temperatureResults: number[];
  pressureResults: number[];
  humidityResults: number[];

  constructor() {
    this.temperatureResults = [];
    this.pressureResults = [];
    this.humidityResults = [];
  }

  updateValues(message: DeviceMsgResponse) {
    switch (message.measure_type) {
      case 0:
        if (message.measured_value)
          this.temperatureResults.push(message.measured_value);
        break;
      case 1:
        if (message.measured_value)
          this.pressureResults.push(message.measured_value);
        break;
      case 2:
        if (message.measured_value)
          this.humidityResults.push(message.measured_value);
        break;
      default:
        console.log("Incorrect measure type");
    }
  }

  getNewestMeasurements() {
    return {
      temperature: this.temperatureResults[this.temperatureResults.length - 1],
      pressure: this.pressureResults[this.pressureResults.length - 1],
      humidity: this.humidityResults[this.humidityResults.length - 1],
    };
  }
}

export default MeasuredValues;
