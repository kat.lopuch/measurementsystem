import React, { useState, useEffect, useRef } from "react";
import "./snackbar.css";

interface SnackbarProps {
  msg: string;
  trigger: boolean;
}

function Snackbar(props: SnackbarProps) {
  const { msg, trigger } = props;
  const [timer, setTimer] = useState<number | null>(null);
  const snackbarRef = useRef<HTMLDivElement>(null);

  const clearTimer = () => {
    window.clearTimeout(timer as number);
    setTimer(null);
    if (snackbarRef.current) {
      snackbarRef.current.classList.remove("snackbar__animation");
      snackbarRef.current.classList.remove("snackbar__opened");
      snackbarRef.current.classList.add("snackbar__closed");
      // Workaround to restart snackbar message display animation
      const stopAnimationWorkaround = snackbarRef.current.offsetHeight;
    }
  };

  useEffect(() => {
    if (timer) clearTimer();

    if (msg !== "" && snackbarRef.current) {
      snackbarRef.current.classList.remove("snackbar__closed");
      snackbarRef.current.classList.add("snackbar__opened");
      snackbarRef.current.classList.add("snackbar__animation");
      setTimer(window.setTimeout(clearTimer, 3000));
    }
  }, [trigger]);

  return (
    <div ref={snackbarRef} className="snackbar__closed">
      <div className="snackbar__content">{msg}</div>
    </div>
  );
}

export default Snackbar;
