function getBaseUrl() {
  return document.URL.split(":")[1];
}

function getBackendUrl() {
  return getBaseUrl() + ":3000";
}

function getBackendUrlWebSocket() {
  return "ws://" + getBaseUrl() + ":3001";
}

function getBackendAuthUrl() {
  return getBaseUrl() + ":4000";
}

module.exports = {
  getBackendUrl,
  getBackendAuthUrl,
  getBackendUrlWebSocket,
};
