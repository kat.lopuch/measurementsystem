function getBackendUrl() {
  return "https://meteo-mars-backend-3vbisao6oa-lz.a.run.app";
}

function getBackendUrlWebSocket() {
  return "wss://meteo-mars-backend-3vbisao6oa-lz.a.run.app/websocket-app/";
}

function getBackendAuthUrl() {
  return "https://meteo-mars-backend-auth-3vbisao6oa-lz.a.run.app";
}

module.exports = {
  getBackendUrl,
  getBackendAuthUrl,
  getBackendUrlWebSocket,
};
