import React from "react";
import { useAppSelector } from "../tools/redux/hooks";
import { AnimatePresence, motion } from "framer-motion";
import DeviceMsgResponseRow from "./deviceMsgResponse_row";
import "./deviceMsgResponses.css";

export default function DeviceMsgResponses() {
  const deviceMsgResponses = useAppSelector(
    (state) => state.deviceMsgResponsesSlice.deviceMsgResponses
  );

  return (
    <div className="deviceMsgResponses">
      <p className="deviceMsgResponses__title">Response messages:</p>
      {deviceMsgResponses.map((deviceMsgResponse, index) => (
        <AnimatePresence key={deviceMsgResponse._id}>
          <motion.div
            initial={{ x: 5, opacity: 0 }}
            animate={{
              opacity: 1,
              x: 0,
              transition: {
                type: "spring",
                duration: 1.5,
                delay: 0.05 * index,
              },
            }}
          >
            <p
              key={deviceMsgResponse._id}
              className="deviceMsgResponses__entry"
            >
              {index + 1}. <DeviceMsgResponseRow message={deviceMsgResponse} />
            </p>
          </motion.div>
        </AnimatePresence>
      ))}
    </div>
  );
}
