import React from "react";
import MessageResponseFromDevice from "../tools/message/interfaces/messageResponse";
import ResponseAdapterText from "../tools/response_adapter_text";
import "./deviceMsgResponse_row.css";

interface DeviceMsgResponseRow {
  message: MessageResponseFromDevice;
}

function DeviceMsgResponseRow(props: DeviceMsgResponseRow) {
  const { message } = props;
  const messageTypeText = ResponseAdapterText.GetMessageTypeAsText(
    message.message_type
  );
  const operationTypeText = ResponseAdapterText.GetOperationTypeAsText(
    message.operation_type
  );
  const statusText = ResponseAdapterText.GetStatusTypeAsText(message.status);

  let result: JSX.Element = <></>;
  if (message.measure_type !== undefined) {
    const measureTypeText = ResponseAdapterText.GetMeasureTypeAsText(
      message.measure_type
    );
    result = (
      <>
        <span className="deviceMsgResponse-row-measureType">
          <label className={colorOfBackground()}>Measure type</label>
          <b>{measureTypeText}</b>
        </span>
        <span>
          <label className={colorOfBackground()}>Measured value</label>
          <b>{message.measured_value}</b>
        </span>
      </>
    );

    function colorOfBackground() {
      if (measureTypeText === "temperature")
        return "deviceMsgResponse-row-temperatureValue";
      if (measureTypeText === "pressure")
        return "deviceMsgResponse-row-pressureValue";
      if (measureTypeText === "humidity")
        return "deviceMsgResponse-row-humidityValue";
    }
  }

  return (
    <span className="deviceMsgResponse-row">
      <span>
        <label className="deviceMsgResponse-row-label">Message type</label>
        <b>{messageTypeText}</b>
      </span>
      <span className="deviceMsgResponse-row-operationType">
        <label className="deviceMsgResponse-row-label operationType">
          Operation type
        </label>
        <b>{operationTypeText}</b>
      </span>
      <span>
        <label className="deviceMsgResponse-row-label status">
          Status of operation:
        </label>
        <b>{statusText}</b>
      </span>
      {result}
    </span>
  );
}

export default DeviceMsgResponseRow;
