import React from "react";
import "./slider_btn.css";
import Arrow from "../../../assets/down-chevron.png";

interface SliderBtnProps {
  direction: string;
  moveSlide: () => void;
}

export default function SliderBtn(props: SliderBtnProps) {
  return (
    <button onClick={props.moveSlide} className="slider__button">
      <img className={props.direction} src={Arrow} alt="Arrow icon" />
    </button>
  );
}
