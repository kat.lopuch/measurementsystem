import React from "react";
import { useState } from "react";
import LightBulb from "../../../assets/light-bulb1.svg";
import Bulb from "../../../assets/bulb.svg";
import Slider from "../slider";
import "./bulb_widget.css";

interface BulbWidgetProps {
  showSliderWidget: boolean;
  toggleSliderWidget: () => void;
}

export default function BulbWidget(props: BulbWidgetProps) {
  const [bulbOn, setBulbOn] = useState(false);

  const showHideSlider = () => {
    let bulbWidgetFrameClass = "bulb_widget__frame ";
    return (bulbWidgetFrameClass += props.showSliderWidget
      ? "no-display"
      : "display");
  };

  return (
    <div className="bulb_widget__row">
      <div className={showHideSlider()}>
        <div className="bulb_widget__container">
          {!props.showSliderWidget && <Slider intervalTime={6000} />}
        </div>
      </div>
      <button
        className="bulb_widget__btn"
        onMouseOver={() => setBulbOn(true)}
        onMouseOut={() => setBulbOn(false)}
        onClick={props.toggleSliderWidget}
      >
        <img
          className="bulb_widget__bulb"
          src={bulbOn ? LightBulb : Bulb}
          alt="bulb"
        />
      </button>
    </div>
  );
}
