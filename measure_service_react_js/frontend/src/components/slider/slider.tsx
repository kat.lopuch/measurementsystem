import React from "react";
import { useEffect, useRef, useState } from "react";
import LightBulb from "../../assets/light-bulb1.svg";
import Bulb from "../../assets/bulb.svg";
import SliderBtn from "../slider/slider_btns/slider_btn";
import { TRIVIA } from "./trivia";
import "./slider.css";
import { useInView } from "framer-motion";

interface SliderProps {
  intervalTime: number;
}

export default function Slider(props: SliderProps) {
  const [slideIndex, setSlideIndex] = useState(0);
  const [bulbOn, setBulbOn] = useState(false);

  const sliderRef = useRef(null);
  const isInView = useInView(sliderRef);

  const nextSlide = () => {
    const nextIndex = slideIndex + 1;
    setSlideIndex(nextIndex < TRIVIA.length ? nextIndex : 0);
  };

  const prevSlide = () => {
    const prevIndex = slideIndex - 1;
    setSlideIndex(prevIndex >= 0 ? prevIndex : TRIVIA.length - 1);
  };

  useEffect(() => {
    let slideInterval = setInterval(nextSlide, props.intervalTime);
    return () => window.clearInterval(slideInterval);
  }, [slideIndex]);

  return (
    <div
      style={{
        transform: isInView ? "none" : "translateX(-100px)",
        opacity: isInView ? 1 : 0,
      }}
      ref={sliderRef}
      className="slider__container"
      onMouseOver={() => setBulbOn(true)}
      onMouseOut={() => setBulbOn(false)}
    >
      <h3 className="slider_header">
        Did you know?
        <img
          className="slider__light-bulb"
          src={bulbOn ? LightBulb : Bulb}
          alt="bulb"
        />
      </h3>

      <div className="slider__trivia-box">
        <SliderBtn moveSlide={prevSlide} direction={"slider_btn__leftArrow"} />
        <p className="slider__trivia_paragraph">{TRIVIA[slideIndex]}</p>
        <SliderBtn moveSlide={nextSlide} direction={"slider_btn__rightArrow"} />
      </div>
    </div>
  );
}
