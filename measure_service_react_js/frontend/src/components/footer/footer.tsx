import React from "react";
import linkedinIcon from "../../assets/linkedin.png";
import gitlabIcon from "../../assets/gitlab.png";
import emailIcon from "../../assets/email.png";
import FooterConnectionStatus from "../footer/footer_connection_status/footer_connection_status";
import "./footer.css";

interface FooterProps {
  connected: boolean;
}

function Footer(props: FooterProps) {
  return (
    <footer>
      <div className="footer__contactWrapper">
        <a
          href="https://www.linkedin.com/in/katarzyna-%C5%82opuch-b9612318a"
          target="_blank"
          rel="noopener noreferrer"
          className="footer__linkedinWrapper"
        >
          <img
            className="footer__contact-icon footer__linkedin"
            src={linkedinIcon}
            alt="linkedIn icon"
          />
        </a>
        <a
          href="https://gitlab.com/users/kat.lopuch/projects"
          target="_blank"
          rel="noopener noreferrer"
          className="footer__gitlabWrapper"
        >
          <img
            className="footer__contact-icon"
            src={gitlabIcon}
            alt="GitLab icon"
          />
        </a>
        <a href="mailto:kat.lopuch@gmail.com" className="footer__emailWrapper">
          <img
            className="footer__contact-icon footer__email"
            src={emailIcon}
            alt="email icon"
          />
        </a>
      </div>
      <FooterConnectionStatus connected={props.connected} />
    </footer>
  );
}

export default Footer;
