import React from "react";
import ConnectionOnIcon from "../../../assets/connection-on.svg";
import ConnectionOffIcon from "../../../assets/connection-off.svg";
import "./footer_connection_status.css";

interface FooterConnectionStatusProps {
  connected: boolean;
}

function FooterConnectionStatus(props: FooterConnectionStatusProps) {
  const getIcon = () => {
    return props.connected ? ConnectionOnIcon : ConnectionOffIcon;
  };

  return (
    <div className="footer_connection_status__wrapper">
      Connection
      <img
        className="footer_connection_status__icon"
        src={getIcon()}
        alt="Login icon"
      />
      <span
        className={
          props.connected
            ? "footer_connection_status__text"
            : "footer_connection_status__hidden"
        }
      >
        OK
      </span>
    </div>
  );
}

export default FooterConnectionStatus;
