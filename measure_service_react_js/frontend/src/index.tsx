import React from "react";
import ReactDOM from "react-dom/client";
import ViewMain from "./views/view_main/view_main";
import { deviceMsgResponsesStorage } from "./components/tools/redux/store";
import { Provider } from "react-redux";
import "./index.css";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <Provider store={deviceMsgResponsesStorage}>
    <ViewMain />
  </Provider>
);
