import React from "react";
import MsgBtns from "../../components/msg_btns/msg_btns";
import Slider from "../../components/slider/slider";
import BulbWidget from "../../components/slider/bulb_widget/bulb_widget";
import DeviceMsgResponses from "../../components/deviceMsgResponses/deviceMsgResponses";
import "./view_advanced.css";

interface ViewAdvancedProps {
  onShowMsg: (text: string) => void;
  onIsLoggedIn: (value: React.SetStateAction<boolean>) => void;
}

interface ViewAdvancedState {
  showSliderWidget: boolean;
}

class ViewAdvanced extends React.Component<
  ViewAdvancedProps,
  ViewAdvancedState
> {
  constructor(props: ViewAdvancedProps) {
    super(props);
    this.state = {
      showSliderWidget: true,
    };
  }

  toggleSliderWidget = () => {
    this.setState({
      showSliderWidget: !this.state.showSliderWidget,
    });
  };

  render() {
    return (
      <div className="view-advanced" id="view-advanced">
        <div className="view-advanced__left-column">
          <MsgBtns
            onShowMsg={this.props.onShowMsg}
            onIsLoggedIn={this.props.onIsLoggedIn}
          />
          <div className="view-advanced__slider-container">
            <Slider intervalTime={6000} />
          </div>
        </div>
        <div className="view-advanced__right-column">
          <DeviceMsgResponses />
        </div>

        <BulbWidget
          showSliderWidget={this.state.showSliderWidget}
          toggleSliderWidget={this.toggleSliderWidget}
        />
      </div>
    );
  }
}

export default ViewAdvanced;
