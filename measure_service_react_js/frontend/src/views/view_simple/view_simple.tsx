import React from "react";
import DeviceMsgResponses from "../../components/deviceMsgResponses/deviceMsgResponses";
import SendMessages from "../../components/tools/message/send_messages";
import WidgetWeather from "../../components/widget_weather/widget_weather";
import "./view_simple.css";

interface ViewSimpleProps {
  showDeviceMsgResponses: boolean;
}

class ViewSimple extends React.Component<ViewSimpleProps> {
  timerRequestMeas: number;
  constructor(props: ViewSimpleProps) {
    super(props);
    this.timerRequestMeas = 0;
  }

  componentDidMount() {
    SendMessages.measureAll();
    this.timerRequestMeas = window.setInterval(
      SendMessages.measureAll,
      3 * 1000
    ); // 3s
  }

  componentWillUnmount() {
    window.clearInterval(this.timerRequestMeas);
  }

  getWidgetClass() {
    return this.props.showDeviceMsgResponses
      ? "widget-weather__small"
      : "widget-weather__big";
  }

  render() {
    return (
      <div className="view-simple">
        <WidgetWeather class={this.getWidgetClass()} />
        {this.props.showDeviceMsgResponses && <DeviceMsgResponses />}
      </div>
    );
  }
}

export default ViewSimple;
