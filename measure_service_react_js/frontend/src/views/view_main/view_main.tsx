import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Header from "../../components/header/header";
import Footer from "../../components/footer/footer";
import Navbar from "../../components/navbar/navbar";
import ViewAdvanced from "../view_advanced/view_advanced";
import ViewSimple from "../view_simple/view_simple";
import Backdrop from "../../components/login/backdrop";
import Login from "../../components/login/login";
import Snackbar from "../../components/snackbar/snackbar";
import {
  getBackendAuthUrl,
  getBackendUrlWebSocket,
} from "../../components/config/config";
import { useAppDispatch } from "../../components/tools/redux/hooks";
import { updateDeviceMsgResponses } from "../../components/tools/redux/deviceMsgResponsesSlice";
import "./view_main.css";

const reconnectionTimeout = 5000;

function ViewMain() {
  const [showDeviceMsgResponses, setShowDeviceMsgResponses] = useState(false);
  const [showLogin, setShowLogin] = useState(false);
  const [snackbarData, updateSnackbar] = useState({ msg: "", trigger: true });
  const [isLoggedIn, setIsLoggedIn] = useState(Login.IsUserLoggedIn());

  let [socketClient, setSocketClient] = useState<WebSocket | null>(null);
  let [reconnecting, setReconnecting] = useState<boolean>(false);
  let [connected, setConnected] = useState<boolean>(false);
  let [timerId, setTimerId] = useState<NodeJS.Timeout | null>(null);
  const dispatch = useAppDispatch();

  const onReconnectionTimeout = () => {
    setReconnecting(true);
    setTimerId(null);
  };

  const initSocket = () => {
    setReconnecting(false);
    const newSocketClient = new WebSocket(getBackendUrlWebSocket());
    setSocketClient(newSocketClient);

    newSocketClient.onopen = () => {
      setConnected(true);
    };

    newSocketClient.onmessage = (result: MessageEvent) => {
      dispatch(
        updateDeviceMsgResponses({
          deviceMsgResponses: JSON.parse(result.data),
        })
      );
    };

    newSocketClient.onclose = () => {
      setSocketClient(null);
      setConnected(false);
      console.log(`Reconnection in ${reconnectionTimeout / 1000}s`);
      setTimerId(setTimeout(onReconnectionTimeout, reconnectionTimeout));
    };
  };

  useEffect(() => {
    if (reconnecting) initSocket();
  }, [reconnecting]);

  useEffect(() => {
    initSocket();

    return () => {
      if (timerId) clearTimeout(timerId);
      if (!socketClient) return;

      socketClient.onerror = () => {
        /* empty arrow function to prevent reconnection on error as the component going to be removed */
      };
      socketClient.onmessage = (result: MessageEvent) => {
        /* empty arrow function to prevent set state on message as the component going to be removed */
      };
      socketClient.onclose = () => {
        /* empty arrow function to prevent reconnection on close as the component going to be removed */
      };
      socketClient.close();
      setSocketClient(null);
      setReconnecting(false);
    };
  }, []);

  const showOnSnackbar = (text: string) => {
    updateSnackbar({ msg: text, trigger: !snackbarData.trigger });
  };

  const logOut = () => {
    const address = getBackendAuthUrl();
    fetch(address + "/logout", {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "refresh-token": localStorage.getItem("refreshToken") || "",
      },
    });
    localStorage.removeItem("accessToken");
    localStorage.removeItem("refreshToken");
    showOnSnackbar("You are logged out");
    setIsLoggedIn(false);
  };

  return (
    <div className="view-main">
      {showLogin && <Backdrop onClick={() => setShowLogin(false)} />}
      {showLogin && (
        <Login
          onHide={() => setShowLogin(false)}
          onShowMsg={showOnSnackbar}
          onIsLoggedIn={setIsLoggedIn}
        />
      )}
      <Snackbar msg={snackbarData.msg} trigger={snackbarData.trigger} />
      <Header />
      <Router>
        <Navbar
          showDeviceMsgResponses={showDeviceMsgResponses}
          onShowDeviceMsgResponses={setShowDeviceMsgResponses}
          onLogin={() => setShowLogin(true)}
          onLogout={logOut}
          isLoggedIn={isLoggedIn}
        />
        <Routes>
          <Route
            path="/"
            element={
              <ViewAdvanced
                onShowMsg={showOnSnackbar}
                onIsLoggedIn={setIsLoggedIn}
              />
            }
          />
          <Route
            path="/simple"
            element={
              <ViewSimple showDeviceMsgResponses={showDeviceMsgResponses} />
            }
          />
        </Routes>
      </Router>
      <Footer connected={connected} />
    </div>
  );
}

export default ViewMain;
