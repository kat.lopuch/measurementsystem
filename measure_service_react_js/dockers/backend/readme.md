# Docker

## Creating docker image of measurement backend:
  1. in terminal, change directory to the folder 
*`measurementsystem`*

2.  the flag `-f measure_service_react_js/dockers/backend/Dockerfile ` allows having the Dockerfile outside the build context in project/measurementsystem
   
3. create image with command
```
$ sudo docker build -t measurement-system-backend:1.0 -f measure_service_react_js/dockers/backend/Dockerfile .
```

## Starting image:

1. change directory to 
*`measurementsystem/measure_service_react_js/dockers/backend`*
2. start image with predefined settings in docker-composed.yaml file
```
$ sudo docker-compose -f docker-composed.yaml up
```