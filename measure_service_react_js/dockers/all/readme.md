# Docker

## Creating docker image:
1. in terminal, change directory to the folder *`measurementsystem`*
2.  the flag `-f measure_service_react_js/dockers/all/Dockerfile ` allows having the Dockerfile outside the build context in project/measurementsystem
3. create image with command
```
$ sudo docker build -t measurement-system:1.1 -f measure_service_react_js/dockers/all/Dockerfile .
```

## Starting image

1. change directory to 
*`measurementsystem/measure_service_react_js/dockers/all`*
2. start image with predefined settings in docker-composed.yaml file
```
$ sudo docker-compose -f docker-composed.yaml up
```