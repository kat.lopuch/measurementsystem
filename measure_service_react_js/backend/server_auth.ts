// ----------------------------------------
// ------------ auth server ---------------
// ----------------------------------------

import express, { Request, Response } from "express";
import cors from "cors";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
require("dotenv").config();
const app = express();
app.use(express.json());
app.use(cors());

let REFRESH_TOKENS: string[] = [];

interface User {
  name: string;
}

const DUMMY_USER = {
  login: "admin",
  password: bcrypt.hashSync("password", 10),
};

app.get("/", (req, res) => {
  res.send("<p>Meteo Mars Backend Auth!</p>");
});

app.post("/login", async (req: Request, res: Response) => {
  if (DUMMY_USER.login != req.body.username)
    return res.status(400).json({ status: "Incorrect user" });

  if (!req.body.psw) return res.status(400).json({ status: "Incorrect user" });

  try {
    const refreshToken: string | undefined = generateRefreshToken({
      name: req.body.username,
    });
    if (refreshToken === undefined) throw "Refresh token generation error";

    REFRESH_TOKENS.push(refreshToken);
    if (bcrypt.compareSync(req.body.psw, DUMMY_USER.password)) {
      res.json({
        accessToken: generateAccessToken({ name: req.body.username }),
        refreshToken: refreshToken,
      });
    } else {
      res.status(400).json({ status: "Incorrect user" });
    }
  } catch (exception) {
    console.log(exception);
    res.status(500).json({ status: "Internal server error" });
  }
});

app.post("/refreshtoken", async (req: Request, res: Response) => {
  const refreshToken: string = req.headers["refresh-token"] as string;
  if (refreshToken == null) return res.sendStatus(401);
  if (!REFRESH_TOKENS.includes(refreshToken)) return res.sendStatus(403);
  if (!process.env.REFRESH_TOKEN_SECRET) return res.sendStatus(403);

  jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
    if (err) return res.sendStatus(403);
    if (!user) return res.sendStatus(403);
    if (typeof user === "string") return res.sendStatus(403);
    if (!user.name) return res.sendStatus(403);

    const accessToken: string | undefined = generateAccessToken({
      name: user.name,
    });
    if (accessToken === undefined) return res.sendStatus(403);

    res.json({ accessToken: accessToken });
  });
});

app.delete("/logout", (req: Request, res: Response) => {
  const refreshToken = req.headers["refresh-token"];
  if (!refreshToken) {
    return res.sendStatus(500);
  }
  REFRESH_TOKENS = REFRESH_TOKENS.filter((token) => token !== refreshToken);
  res.sendStatus(204);
});

function generateAccessToken(user: User) {
  if (process.env.ACCESS_TOKEN_SECRET)
    return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, {
      expiresIn: "10m",
    });
}

function generateRefreshToken(user: User) {
  if (process.env.REFRESH_TOKEN_SECRET)
    return jwt.sign(user, process.env.REFRESH_TOKEN_SECRET, {
      expiresIn: "2d",
    });
}

var port = process.env.PORT || 4000; // take port from gcloud env or 4000
app.listen(port, function () {
  console.log("[app auth] listening on port: ", port);
});
