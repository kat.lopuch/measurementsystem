import DeviceMessengerNoDB from "./DeviceMessengerNoDB";
import DeviceMessengerMongo from "./DeviceMessengerMongo";

export default function createDeviceMessenger(
  savingMessagesMode: string | undefined
) {
  if ("mongodb" == savingMessagesMode) {
    return new DeviceMessengerMongo();
  }

  return new DeviceMessengerNoDB();
}
