import * as udpSocket from "dgram";
import { ObjectId } from "mongodb";
import WebSocketWrapper from "./WebsocketWrapper";
import MessageRequest from "../types/messageRequest";
import MessageResponse from "../types/messageResponse";

const maxNumberOfMessages: number = 50;

export default class DeviceMessengerNoDB {
  server: udpSocket.Socket;
  client: udpSocket.Socket;
  MESSAGE_RESPONSES: MessageResponse[];
  webSocket: WebSocketWrapper;

  constructor() {
    this.server = udpSocket.createSocket("udp4");
    this.client = udpSocket.createSocket("udp4");
    this.MESSAGE_RESPONSES = [];
    this.listenForMessages();
    this.webSocket = new WebSocketWrapper(async () => this.getAll());
  }

  /**
   *  @brief message is supposed to be an object with properties rather than JSON string
   */
  sendMessage(message: MessageRequest) {
    // converting object to json string and than to buffer with ASCII values 0-255
    const data = Buffer.from(JSON.stringify(message));
    this.client.send(data, 8090, "localhost", (error) => {
      if (error) this.client.close();
    });
  }

  listenForMessages() {
    this.server.on("message", (msg) => {
      console.log("[SERVER UDP] Data received from device : " + msg.toString());
      this.MESSAGE_RESPONSES.unshift({
        _id: new ObjectId(),
        ...JSON.parse(msg.toString()),
      });

      if (this.MESSAGE_RESPONSES.length > maxNumberOfMessages)
        this.MESSAGE_RESPONSES.pop();

      this.webSocket.broadcastData(this.MESSAGE_RESPONSES);
    });
    this.server.bind(8080);
  }

  /**
   * @return list of message objects with properties rather than strings formatted as JSON
   */
  async getAll() {
    return this.MESSAGE_RESPONSES;
  }

  removeAll() {
    this.MESSAGE_RESPONSES.length = 0;
    this.webSocket.broadcastData(this.MESSAGE_RESPONSES);
  }
}
