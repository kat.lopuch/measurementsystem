import getAddress from "../config";
import * as WebSocket from "ws";

import MessageResponse from "../types/messageResponse";

export default class WebSocketWrapper {
  clients: WebSocket[];
  getData: () => Promise<MessageResponse[]>;
  wss: WebSocket.Server;

  constructor(getData: () => Promise<MessageResponse[]>) {
    this.clients = [];
    this.getData = getData;
    this.wss = new WebSocket.Server({ port: 3001, host: getAddress() });
    this.wss.on("connection", this.handleConnection.bind(this));
  }

  async handleConnection(client: WebSocket) {
    console.log("New connection");
    this.clients.push(client);

    const endClient = () => {
      let position: number = this.clients.indexOf(client);
      this.clients.splice(position, 1);
      console.log("Connection closed");
    };

    client.on("close", endClient);

    // At the beginning, send data to the freshly connected client.
    client.send(JSON.stringify(await this.getData()));
  }

  broadcastData(data: MessageResponse[]) {
    for (const client of this.clients) client.send(JSON.stringify(data));
  }
}
