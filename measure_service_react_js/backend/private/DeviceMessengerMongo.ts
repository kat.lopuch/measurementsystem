import * as udpSocket from "dgram";
import WebSocket from "./WebsocketWrapper";
import MessageRequest from "../types/messageRequest";
import MessageResponse from "../types/messageResponse";
import * as mongoDB from "mongodb";
import WebSocketWrapper from "./WebsocketWrapper";
import * as fs from "fs";

const maxNumberOfMessages: number = 50;

function getUrlToMongo(): string {
  let mongoConfig: { MONGOURL: string } = JSON.parse(
    fs.readFileSync("mongo_config.json").toString()
  );

  return mongoConfig.MONGOURL
    ? mongoConfig.MONGOURL
    : "mongodb://root:password@localhost";
}

const databaseName: string = "meteo-mars";
const mongoClientOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
} as mongoDB.MongoClientOptions;

export default class DeviceMessengerMongo {
  server: udpSocket.Socket;
  client: udpSocket.Socket;
  mongodbClient: mongoDB.MongoClient;
  webSocket: WebSocket;
  messagesCollection: mongoDB.Collection<MessageResponse>;
  changeStream: mongoDB.ChangeStreamDocument | mongoDB.Document;

  constructor() {
    this.server = udpSocket.createSocket("udp4");
    this.client = udpSocket.createSocket("udp4");
    this.mongodbClient = new mongoDB.MongoClient(
      getUrlToMongo(),
      mongoClientOptions
    );
    this.webSocket = new WebSocketWrapper(async () => this.getAll());

    this.messagesCollection = this.mongodbClient
      .db(databaseName)
      .collection<MessageResponse>("messages");
    this.changeStream = this.messagesCollection.watch();
    this.changeStream.on("change", async () => {
      this.webSocket.broadcastData(await this.getAll());
    });

    this.listenForMessages();
  }

  sendMessage(message: MessageRequest) {
    const data = Buffer.from(JSON.stringify(message));
    this.client.send(data, 8090, "localhost", (error) => {
      if (error) {
        console.log("[Server][send-message] error:", error);
        this.client.close();
      } else {
        console.log("[Server][send-message] Data sent !!!");
      }
    });
  }

  listenForMessages() {
    this.server.on("message", async (message) => {
      let messageResponse: MessageResponse = JSON.parse(message.toString());
      messageResponse["insertionDate"] = new Date();
      let messages = this.mongodbClient.db(databaseName).collection("messages");
      messages.insertOne(messageResponse);

      const cursor = messages
        .find({})
        .sort({ insertionDate: -1 })
        .skip(maxNumberOfMessages);
      messages.deleteMany(cursor);
    });
    this.server.bind(8080);
  }

  /**
   * @return list of message objects with properties rather than strings formatted as JSON
   */
  async getAll() {
    const allValues = await this.mongodbClient
      .db(databaseName)
      .collection<MessageResponse>("messages")
      .find()
      .sort({ insertionDate: -1 })
      .limit(maxNumberOfMessages)
      .toArray();

    return allValues;
  }

  removeAll() {
    this.mongodbClient.db(databaseName).collection("messages").deleteMany({});
    this.webSocket.broadcastData([]);
  }
}
