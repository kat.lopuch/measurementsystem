# Backend

## Database - MongoDB

MongoDB is started using docker images. To automate the setup docker-compose.yml file is used.

docker-compose.yml creates two services:
- mongodb-service
- mongo-express-service

## Starting image

1. in terminal, change directory to the folder
```
$ cd measurementsystem/measure_service_react_js/backend
```  
2. create containers with the following command
```
$ docker-compose up
```