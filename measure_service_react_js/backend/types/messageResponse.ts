import { ObjectId } from "mongodb";

export default interface MessageResponse {
  _id: ObjectId;
  message_type: number;
  operation_type: number;
  status: number;
  measure_type?: number;
  measured_value?: number;
  insertionDate?: Date;
}
