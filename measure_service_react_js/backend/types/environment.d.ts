export {};

declare global {
  namespace NodeJS {
    interface ProcessEnv {
      ACCESS_TOKEN_SECRET: string;
      REFRESH_TOKEN_SECRET: string;
      SAVING_MESSAGES_MODE: string;
      DEPLOYMENT_TYPE: string;
    }
  }
}
