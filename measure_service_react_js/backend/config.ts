require("dotenv").config();

let address: string = "127.0.0.10";

export default function getAddress() {
  if (process.env.DEPLOYMENT_TYPE === "local") return "localhost";
  return address;
}
