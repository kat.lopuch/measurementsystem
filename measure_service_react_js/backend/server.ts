import express, { Request, Response } from "express";
import cors from "cors";
import jwt from "jsonwebtoken";
import createDeviceMessenger from "./private/DeviceMessengerFactory";
import getAddress from "./config";

const app = express();
app.use(express.json());
app.use(cors());

let deviceMessenger = createDeviceMessenger(process.env.SAVING_MESSAGES_MODE);

app.get("/", (req: Request, res: Response) => {
  res.send("<p>Meteo Mars Backend!</p>");
});

app.post("/device-request-message", function (req: Request, res: Response) {
  if (req.body.operation_type === null) {
    res.status(400).json({ status: "Empty message to device" });
    return;
  }

  if (req.body.operation_type === 3) {
    if (!verifyToken(req.headers["authorization"])) {
      res.status(401).json({ status: "Access not allowed" });
      return;
    }
  }

  deviceMessenger.sendMessage(req.body);

  res.status(200).json({ status: "Done" });
});

app.delete("/device-response-messages", (req: Request, res: Response) => {
  deviceMessenger.removeAll();
  res.status(200).json({ status: "Done" });
});

app.get(
  "/device-response-messages",
  async function (req: Request, res: Response) {
    const jsonMessages: string[] = [];
    for (const message of await deviceMessenger.getAll())
      jsonMessages.push(JSON.stringify(message));

    res.send(jsonMessages);
  }
);

/////////

function verifyToken(authHeader: string | undefined) {
  if (authHeader === undefined) return;

  const token: string | null = authHeader && authHeader.split(" ")[1];
  if (token === null) return false;
  if (!process.env.ACCESS_TOKEN_SECRET) return false;

  return jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (error) =>
    error ? false : true
  );
}

app.listen(3000, getAddress(), function () {
  console.log("[app] listening on:", getAddress() + ":" + 3000);
});
